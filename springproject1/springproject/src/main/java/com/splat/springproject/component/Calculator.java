package com.splat.springproject.component;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;


/**
 *
 *Calculator для подсчета ежемесячного платежа по аннуитетному кредиту
 */
@Component
public class Calculator {
    public static double annuiteMonthlyPayments(double S, double P, int m){
        BigDecimal PB = BigDecimal.valueOf(P);
        BigDecimal SB = BigDecimal.valueOf(S);
        return annuiteMonthlyPayments(SB, PB, m);
    }
    public static double annuiteMonthlyPayments(BigDecimal S, BigDecimal P, int m){
        S = S.setScale(6);
        P = P.setScale(6);
        BigDecimal p = P.divide(new BigDecimal("12").setScale(6), BigDecimal.ROUND_HALF_EVEN);
        p = p.divide(new BigDecimal("100").setScale(6), BigDecimal.ROUND_HALF_EVEN);
        BigDecimal K1 = p.add(new BigDecimal("1")).pow(m).multiply(p);
        BigDecimal K2 = p.add(new BigDecimal("1")).pow(m).subtract(new BigDecimal("1"));
        BigDecimal K = K1.divide(K2, BigDecimal.ROUND_HALF_EVEN);
        return S.multiply(K).setScale(2,BigDecimal.ROUND_HALF_EVEN).doubleValue();
    }
}

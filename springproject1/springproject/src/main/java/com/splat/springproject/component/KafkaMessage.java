package com.splat.springproject.component;

import com.splat.springproject.entity.Request;
import com.splat.springproject.repo.RequestRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class KafkaMessage {
    @Autowired
    public void setRequestRepo(RequestRepo requestRepo) {
        this.requestRepo = requestRepo;
    }

    private RequestRepo requestRepo;
    @KafkaListener(topics = "getZapros")
    public void handle(String str){
        String[] arr = str.split(" ");
        if(arr.length==2) {
            Optional<Request> req = requestRepo.findById(Long.parseLong(arr[0]));
            if ((req.isPresent()) && (arr.length == 2)) {
                req.get().setNalog(Long.parseLong(arr[1]));
                req.get().setStatus("На рассмотрении");
            }
            //System.out.println("mess" + req.get().getNalog());
            requestRepo.save(req.get());
        }
        //System.out.println("mess" + str);

    }
}

package com.splat.springproject.component;

import com.splat.springproject.entity.Request;
import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class ReqComponent {
    private Request request;
}

package com.splat.springproject.component;

import lombok.Data;

import java.util.Date;
@Data
public class SheduleClass {
    private String date;
    private double summ;
}

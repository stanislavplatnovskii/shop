package com.splat.springproject.controller;

import com.splat.springproject.entity.Credit;
import com.splat.springproject.entity.Request;
import com.splat.springproject.repo.RequestRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/allrequest")
public class AllRequestController {
    private KafkaTemplate<String,String> kafkaTemplate;
    @Autowired
    public void setKafkaTemplate(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }



    @Autowired
    public void setRequestRepo(RequestRepo requestRepo) {
        this.requestRepo = requestRepo;
    }

    private RequestRepo requestRepo;
    @ModelAttribute
    public  void addAtr(Model model){
        Iterable<Request> credits = requestRepo.findAll();
        model.addAttribute("credits",credits);
    }

    @GetMapping
    public String fun(){
        return "listReq";
    }

    @GetMapping("/send")
    public String fun2(){return "send";}

    @PostMapping("/send")
    public String addReq(@RequestParam("Id") String id){
        Optional<Request> byId = requestRepo.findById(Long.parseLong(id));
        if(byId.isPresent()){
            Request req = byId.get();
            String str=req.getId() + " " + req.getLastName()+" "+req.getFirstName()+" " + req.getPatron();
            //System.out.println(req.getLastName());
            kafkaTemplate.send("zapros", str);
            req.setStatus("Проверка данных");
            requestRepo.save(req);
            return "redirect:/allrequest";
        }else {
            //rep.save(req);
            return "send";
        }
    }

}

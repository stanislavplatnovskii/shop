package com.splat.springproject.controller;

import com.splat.springproject.component.Calculator;
import com.splat.springproject.component.ReqComponent;
import com.splat.springproject.component.SheduleClass;
import com.splat.springproject.entity.Credit;
import com.splat.springproject.entity.Request;
import com.splat.springproject.entity.User;
import com.splat.springproject.repo.CreditRepo;
import com.splat.springproject.repo.RequestRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.validation.Valid;
import java.util.*;

@Controller
@RequestMapping("/credits")
@SessionAttributes("req")
public class CreditController {
    private CreditRepo repoCredit;
    private RequestRepo reqRepo;


    @Autowired
    public void setReqRepo(RequestRepo reqRepo) {
        this.reqRepo = reqRepo;
    }


    @Autowired
    public void setRepoCredit(CreditRepo repoCredit) {
        this.repoCredit = repoCredit;
    }

    @ModelAttribute(name = "req")
    public Request req(){
        return new Request();
    }



    @ModelAttribute
    public  void getCredits(Model model, SessionStatus sessionStatus, @AuthenticationPrincipal User user){
        List<Credit> listCredits = new ArrayList<>();
        Iterable<Credit> credits = repoCredit.findAll();
        model.addAttribute("credits",credits);
        Iterable<Request> creditsUser = requestRepo.findByUser(user);
        model.addAttribute("creditsUser",creditsUser);

    }


    @GetMapping("/new")
    public String showCredits(){
        return "credits";
    }
    @PostMapping("/new")
    public String addReq(@ModelAttribute(name = "req") Request req, Errors errors, SessionStatus sessionStatus, @AuthenticationPrincipal User user){

        if((req.getCredit()!=null)&&(req.getSum()>=req.getCredit().getMinSum()) &&(req.getSum()<=req.getCredit().getMaxSum())
                &&(req.getDate()>=req.getCredit().getMinDate())&&(req.getDate()<=req.getCredit().getMaxDate())) {


            req.setUser(user);
            req.setFirstName(user.getFirstName());
            req.setLastName(user.getLastName());
            req.setPatron(user.getPatron());
            req.setNomer(user.getPhoneNumber());
            req.setEMail(user.getEMail());
            req.setStatus("Не рассмотрена");
            return "redirect:/payschedule";
        }else{
            return "creditsErr";
        }
    }

    @Autowired
    public void setRequestRepo(RequestRepo requestRepo) {
        this.requestRepo = requestRepo;
    }

    private RequestRepo requestRepo;






    @GetMapping("/all")
    public String showall(){
        return "creditsuser";
    }

}

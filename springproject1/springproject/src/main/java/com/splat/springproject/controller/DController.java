package com.splat.springproject.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/deny")
public class DController {
    @GetMapping
    public String func(){
        return "deny";
    }
}


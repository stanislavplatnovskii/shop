package com.splat.springproject.controller;

import com.splat.springproject.component.Calculator;
import com.splat.springproject.component.ReqComponent;
import com.splat.springproject.component.SheduleClass;
import com.splat.springproject.entity.Request;
import com.splat.springproject.repo.RequestRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

@Controller
@RequestMapping("/payschedule")
@SessionAttributes("req")
public class PayController {
    private Request req;
    private RequestRepo rep;

    @Autowired
    public void setRep(RequestRepo rep) {
        this.rep = rep;
    }



    @ModelAttribute
    public void getSh(Model model){
        GregorianCalendar calendar = new GregorianCalendar();
        List<SheduleClass> list = new ArrayList<>();
        req = (Request) model.getAttribute("req");
        //System.out.println(req.getDate());
        double monS = Calculator.annuiteMonthlyPayments(req.getSum(),req.getCredit().getProcent(), Math.toIntExact(req.getDate()));
        for(int i=0;i<req.getDate();i++){
            calendar.add(Calendar.MONTH, 1);
            SheduleClass s = new SheduleClass();
            s.setDate(calendar.get(Calendar.DAY_OF_MONTH) +"." + (calendar.get(Calendar.MONTH)+1)+ "."+ String.valueOf(calendar.get(Calendar.YEAR)));
            s.setSumm(monS);
            //s.setSumm(5000);
            list.add(s);
        }

        model.addAttribute("shedule", list.iterator());
    }

    @GetMapping
    public String showCredits(){
        return "shedule";
    }

    @PostMapping()
    public String addReq(){
        rep.save(req);
        return "redirect:/yes";
    }
}

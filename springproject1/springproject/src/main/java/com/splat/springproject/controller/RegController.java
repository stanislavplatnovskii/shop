package com.splat.springproject.controller;

import com.splat.springproject.entity.User;
import com.splat.springproject.repo.UserRepo;
import com.splat.springproject.security.RegisForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/register")
public class RegController {

    private UserRepo userRepo;
    private PasswordEncoder passwordEncoder;
    @ModelAttribute(name="regisForm")
    public RegisForm form(){
        return new RegisForm();
    }

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }
    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }
    @GetMapping
    public String regShow(){
        return "registration";
    }
    @PostMapping
    public String saveReg(@Valid RegisForm regisForm, Errors errors){
        if(errors.hasErrors()){
            return "registration";
        }

        User user  = regisForm.toUser(passwordEncoder);
        User findUser = userRepo.findByUsername(user.getUsername());
        if(findUser == null){
        userRepo.save(user);
        return "redirect:/start";
        }else{
            return "redirect:/register";
        }
    }
}

package com.splat.springproject.controller;

import com.splat.springproject.component.ReqComponent;
import com.splat.springproject.entity.Request;
import com.splat.springproject.repo.RequestRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@EnableWebMvc
@Controller
@RequestMapping("/start")
public class RequesController {

    @GetMapping
    public String show(){
        return "start";
    }


}

package com.splat.springproject.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/yes")
public class YesController {
    @GetMapping
    public String fun(){
        return "yes";
    }
}

package com.splat.springproject.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Entity
public class Request {
    private static final long serialVersionUID = 3L;
    @Id
    @GeneratedValue(strategy  = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private User user;
    private String lastName;
    private String firstName;
    private String patron;
    private String nomer;
    private String eMail;
    private String status;
    @OneToOne
    private Credit credit;
    private Long sum;
    private Long date;
    private Long nalog;

}

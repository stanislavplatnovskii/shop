package com.splat.springproject.repo;

import com.splat.springproject.entity.Credit;
import org.springframework.data.repository.CrudRepository;

public interface CreditRepo extends CrudRepository<Credit, Long> {
}

package com.splat.springproject.repo;

import com.splat.springproject.entity.Request;
import com.splat.springproject.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RequestRepo extends CrudRepository<Request, Long> {
    List<Request> findByUser(User user);
    //Request findById(Long Id);
}

package com.splat.springproject.security;

import com.splat.springproject.entity.User;
import lombok.*;
import org.springframework.security.crypto.password.PasswordEncoder;


import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


//@NoArgsConstructor(access= AccessLevel.PUBLIC, force = true)
//@RequiredArgsConstructor
@Data
public class RegisForm {
    @NotBlank(message = "Обязательное для заполнения поле!")
    private String username;
    @NotNull
    @Size(min=8, message = "Минимальная длинна 8 символов")
    private String password;
    @NotBlank(message = "Обязательное для заполнения поле!")
    private String firstName;
    @NotBlank(message = "Обязательное для заполнения поле!")
    private String lastName;
    @NotBlank(message = "Обязательное для заполнения поле!")
    private String patron;
    @NotNull
    @Size(min=11,max=12, message = "Пример 89563545689")
    private String phoneNumber;
    
    @Email(message = "Неверное значение e-mail")
    private String eMail;


    public User toUser(PasswordEncoder passwordEncoder){
        return new User(username, passwordEncoder.encode(password),firstName, lastName,
                patron, phoneNumber, eMail, "USER");
    }

}

package com.splat.springproject.security;

import com.splat.springproject.entity.User;
import com.splat.springproject.repo.UserRepo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class SecurityConfig {

    @Bean
    public UserDetailsService userDetailsService(UserRepo userRepo){
        return username -> {
            User user = userRepo.findByUsername(username);
            if(user != null) return user;

            throw new UsernameNotFoundException(username + " not found");
        };
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception{
        return http
                .authorizeRequests()
                .antMatchers("/credits/new", "/payschedule", "/credits/all").hasRole("USER")
                .antMatchers( "/allrequest","/allrequest/send").hasRole("ADMIN")
                .antMatchers("/","/**").permitAll()
                .and()
                .formLogin().defaultSuccessUrl("/credits/all")
                .and()
                .csrf()
                .disable().build();
                //.and().build();


    }



}
